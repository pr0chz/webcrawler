import net.ruippeixotog.scalascraper.browser._
import net.ruippeixotog.scalascraper.model._
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.dsl.DSL.Extract._

object WebCrawler {

  type URL = String

  def fetchPage(url:URL):Document = JsoupBrowser().get(url)

  def containsPhrase(doc:Document, phrase:String):Boolean = doc.body.text.contains(phrase)

  def nextUrls(doc:Document): Set[URL] = {
    val urls = doc >?> elementList("a") >?> attr("href")("a")
    val s = urls.map(x => x.flatMap(y => y.toList)).get
    s.filter(x => x.startsWith("http") || x.startsWith("https") || x.startsWith(".")).toSet
  }

  def findPagesWithPhrase(depth:Int, url:String, phrase:String): Set[URL] = {
    if (depth == 0) Set()
    else {
      println(s"Fetching $url")
      val doc = fetchPage(url)
      val children = for {
        next <- nextUrls(doc)
        url <- findPagesWithPhrase(depth - 1, next, phrase)
      } yield url

      children ++ (if (containsPhrase(doc, phrase)) Set(url) else Set())
    }
  }

  // enqueue
  // deque
  // pool of crawlers
  // output queue

  def main(args: Array[String]): Unit = {
//    val doc = fetchPage("http://www.seznam.cz")
//    println(nextUrls(doc))
    println(findPagesWithPhrase(5, "http://www.seznam.cz", "sezfdsjfk"))
  }

}
